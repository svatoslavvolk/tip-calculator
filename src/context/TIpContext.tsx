import { createContext, useContext, useState } from "react";

type State = {
  bill: number;
  tip: number | null;
  numberOfPeople: number;
};

type Context = State & {
  setBill: (value: number) => void;
  setTip: (value: number) => void;
  setNumberOfPeople: (value: number) => void;
  resetValues: () => void;
};

const INITIAL_STATE: State = {
  bill: 0,
  tip: null,
  numberOfPeople: 1,
};

export const TipContext = createContext<Context>({
  ...INITIAL_STATE,
  setBill: () => {},
  setNumberOfPeople: () => {},
  setTip: () => {},
  resetValues: () => {},
});

export const TipContextProvider: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const [tipValues, setTipValues] = useState<State>(INITIAL_STATE);

  const setTip = (tip: number) => {
    setTipValues((prev) => ({
      ...prev,
      tip,
    }));
  };

  const setBill = (value: number) => {
    setTipValues((prev) => ({
      ...prev,
      bill: value,
    }));
  };

  const setNumberOfPeople = (value: number) => {
    setTipValues((prev) => ({
      ...prev,
      numberOfPeople: value,
    }));
  };

  const resetValues = () => {
    setTipValues(INITIAL_STATE);
  };

  const value: Context = {
    ...tipValues,
    resetValues,
    setBill,
    setNumberOfPeople,
    setTip,
  };

  return <TipContext.Provider value={value}>{children}</TipContext.Provider>;
};

export const useTipCalculator = () => useContext(TipContext);
