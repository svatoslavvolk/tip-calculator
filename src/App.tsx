import { Grid, Stack } from "@mui/material";
import ResultDialog from "./components/ResultDialog/ResultDialog";
import TipForm from "./components/TipForm/TipForm";
import { TipContextProvider } from "./context/TIpContext";

function App() {
  return (
    <Stack
      sx={{
        height: "100%",
        minHeight: "100vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Grid
        container
        spacing={5}
        sx={{
          bgcolor: "#fff",
          maxWidth: "1080px",
          borderRadius: 6,
          pr: 5,
          pb: 5,
        }}
      >
        <TipContextProvider>
          <Grid item xs={6}>
            <TipForm />
          </Grid>
          <Grid item xs={6}>
            <ResultDialog />
          </Grid>
        </TipContextProvider>
      </Grid>
    </Stack>
  );
}

export default App;
