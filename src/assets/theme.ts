import { ThemeOptions, createTheme } from '@mui/material';

let theme = createTheme({
  palette: {
    primary: {
      300: 'hsl(189, 41%, 97%)',
      light: 'hsl(185, 41%, 84%)',
      main: 'hsl(172, 67%, 45%)',
      600: 'hsl(184, 14%, 56%)',
      dark: 'hsl(186, 14%, 43%)',
      800: 'hsl(183, 100%, 15%)',
    },
  },
  typography: {
    fontFamily: "'Space Mono', monospace",
  },
});

theme = createTheme(theme as ThemeOptions, {
  typography: {
    fontFamily: "'Space Mono', monospace",
  },
  
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          backgroundColor: theme.palette.primary.light,
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: "24px",
          fontWeight: 700,
          "&:hover": { backgroundColor: theme.palette.primary.light },
        }
      }
    }
  },
} as ThemeOptions);

export default theme;
