// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { Theme } from "@mui/material";

declare module '@mui/material/styles' {
	interface Theme {
		palette: {
			primary: {
				300: string;
				light: string;
				main: string;
				dark: string;
				600: string;
				800: string;
			}
			common: {
				white: string;
				dark: string;
			}
		}
	}
}