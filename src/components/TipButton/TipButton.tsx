import { Button, useTheme } from "@mui/material";

type TipButtonProps = {
  tip: number;
  isActive?: boolean;
  onSelectTip: (tip: number) => void;
};

const TipButton: React.FC<TipButtonProps> = ({
  tip,
  isActive,
  onSelectTip,
}) => {
  const theme = useTheme();

  return (
    <Button
      onClick={() => onSelectTip(tip)}
      variant="contained"
      sx={{
        paddingBlock: 0.5,
        paddingInline: 5,
        width: "100%",
        bgcolor: isActive
          ? theme.palette.primary.main
          : theme.palette.primary[800],
        color: theme.palette.common.white,

        "&:hover": { bgcolor: theme.palette.primary.main },
      }}
    >
      {tip}%
    </Button>
  );
};

export default TipButton;
