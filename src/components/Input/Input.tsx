import {
  FormControl,
  InputAdornment,
  OutlinedInput,
  styled,
  useTheme,
} from "@mui/material";

const Label = styled("label")(({ theme }) => ({
  fontWeight: 700,
  color: theme.palette.primary.dark,
  marginBottom: 4,
}));

type InputProps = {
  label: string;
  placeholder: string;
  type: "text" | "email" | "number";
  id: string;
  name: string;
  icon: React.ElementType;
  min?: number;
  max?: number;
  value?: string | number;
  onChange?: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
};

const Input: React.FC<InputProps> = ({
  label,
  placeholder,
  value,
  onChange,
  type = "text",
  id,
  name,
  icon: Icon,
  min,
  max,
}) => {
  const theme = useTheme();
  return (
    <FormControl size="small">
      <Label htmlFor={id}>{label}</Label>
      <OutlinedInput
        value={value}
        onChange={onChange}
        name={name}
        type={type}
        placeholder={placeholder}
        id={id}
        startAdornment={
          Icon && <InputAdornment position="start">{<Icon />}</InputAdornment>
        }
        inputProps={{
          min,
          max,
        }}
        sx={{
          bgcolor: theme.palette.primary[300],
          "& .MuiOutlinedInput-notchedOutline": {
            borderColor: "transparent",
            borderWidth: "2px",
            transition: theme.transitions.create("border-color"),
          },
          "&:hover .MuiOutlinedInput-notchedOutline": {
            borderColor: theme.palette.primary.main,
          },
          "& .MuiInputBase-input": {
            textAlign: "right",
            fontSize: 24,
            fontWeight: 700,
            color: theme.palette.primary[800],
          },
        }}
      />
    </FormControl>
  );
};

export default Input;
