import { Box, Stack, Typography } from "@mui/material";

type BillResultProps = {
  text: string;
  tip: number;
  total?: string;
};

const ResultField: React.FC<BillResultProps> = ({
  text,
  tip,
}: BillResultProps) => {
  return (
    <Stack direction="row" justifyContent="space-between" alignItems="center">
      <Box>
        <Typography
          sx={{
            color: "#fff",
            fontWeight: 700,
          }}
        >
          {text}
        </Typography>
        <Typography
          sx={{
            color: "primary.600",
          }}
        >
          / person
        </Typography>
      </Box>
      <Typography
        sx={{
          fontSize: 40,
          color: "primary.main",
          fontWeight: 700,
        }}
      >
        ${tip}
      </Typography>
    </Stack>
  );
};

export default ResultField;
