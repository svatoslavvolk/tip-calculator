import { Button, Stack } from "@mui/material";
import ResultField from "../ResultField/ResultField";
import { useTipCalculator } from "../../context/TIpContext";

const ResultDialog = () => {
  const { tip, numberOfPeople, bill, resetValues } = useTipCalculator();

  const tipAmountPerPerson = ((bill / 100) * (tip ?? 0)) / numberOfPeople;
  const totalAmount = bill / numberOfPeople + tipAmountPerPerson;

  return (
    <Stack
      justifyContent="space-between"
      sx={{
        p: 5,
        bgcolor: "primary.800",
        borderRadius: 5,
        height: "100%",
      }}
    >
      <Stack spacing={5}>
        <ResultField text="Tip Amount" amount={tipAmountPerPerson} />
        <ResultField text="Total" amount={totalAmount} />
      </Stack>
      <Button onClick={resetValues} variant="contained">
        Reset
      </Button>
    </Stack>
  );
};

export default ResultDialog;
