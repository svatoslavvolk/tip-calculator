import { OutlinedInput, useTheme } from "@mui/material";

type CustomTipProps = {
  placeholder: string;
  value?: string;
  onChangeTip: (tip: number) => void;
};

const CustomTip: React.FC<CustomTipProps> = ({
  placeholder,
  onChangeTip,
  value,
}) => {
  const theme = useTheme();

  return (
    <OutlinedInput
      type="number"
      inputProps={{
        min: 0,
      }}
      value={value}
      onChange={(e) => onChangeTip(Number(e.target.value))}
      placeholder={placeholder}
      sx={{
        bgcolor: theme.palette.primary[300],
        "& .MuiInputBase-input": {
          textAlign: "center",
          fontSize: 24,
          py: 1,
          fontWeight: 700,
          color: theme.palette.primary[800],
        },
        "& .MuiInputBase-input::placeholder": {
          color: theme.palette.primary[800],
          opacity: 0.8,
        },
        "& .MuiOutlinedInput-notchedOutline": {
          borderColor: "transparent",
          borderWidth: "2px",
          transition: theme.transitions.create("border-color"),
        },
        "&:hover .MuiOutlinedInput-notchedOutline": {
          borderColor: theme.palette.primary.main,
        },
      }}
    />
  );
};

export default CustomTip;
