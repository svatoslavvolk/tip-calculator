import { Box, Stack, Typography } from "@mui/material";
import { formatCurrency } from "../../utils/formatCurrency";

type ResultFieldProps = {
  text: string;
  amount: number;
  total?: string;
};

const ResultField: React.FC<ResultFieldProps> = ({
  text,
  amount,
}: ResultFieldProps) => {
  return (
    <Stack direction="row" justifyContent="space-between" alignItems="center">
      <Box>
        <Typography
          sx={{
            color: "#fff",
            fontWeight: 700,
            fontSize: 18,
          }}
        >
          {text}
        </Typography>
        <Typography
          sx={{
            color: "primary.600",
          }}
        >
          / person
        </Typography>
      </Box>
      <Typography
        sx={{
          fontSize: 40,
          color: "primary.main",
          fontWeight: 700,
        }}
      >
        {formatCurrency(amount)}
      </Typography>
    </Stack>
  );
};

export default ResultField;
