import { Box } from "@mui/material";
import ResultField from "../ResultField/ResultField";

const TipResult = () => {
  return (
    <Box
      sx={{
        p: 5,
        bgcolor: "primary.800",
        borderRadius: 5,
      }}
    >
      <ResultField text="Tip Amount" amount={1} />
      <ResultField text="Total" amount={3} />
    </Box>
  );
};

export default TipResult;
