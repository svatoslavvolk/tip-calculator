import { Box, Button, Typography } from "@mui/material";
import { useState } from "react";


const Counter = () => {
  const [count, setCount] = useState(0);

	return (
		<Box>
		<Typography>{count}</Typography>
		<Button onClick={() => setCount(prevCount => prevCount + 1)}>Increase count</Button>
	</Box>
	)
}

export { Counter }