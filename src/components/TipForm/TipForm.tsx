import { Box, Grid, Stack, Typography } from "@mui/material";
import Input from "../Input/Input";
import DollarIcon from "../../assets/icons/icon-dollar.svg?react";
import TipButton from "../TipButton/TipButton";
import CustomTip from "../CustomTip/CustomTip";
import PersonIcon from "../../assets/icons/icon-person.svg?react";
import { useTipCalculator } from "../../context/TIpContext";
import { useEffect, useState } from "react";

const TIPS = [5, 10, 15, 25, 50];

const TipForm = () => {
  const {
    bill,
    tip: selectedTip,
    setBill,
    numberOfPeople,
    setNumberOfPeople,
    setTip,
  } = useTipCalculator();
  const [customTipValue, setCustomTipValue] = useState<number | null>(null);

  const handleSelectTip = (tip: number) => {
    setTip(tip);
    setCustomTipValue(null);
  };

  const handleCustomTipChange = (newTip: number) => {
    setTip(newTip);
    setCustomTipValue(newTip);
  };

  useEffect(() => {
    if (selectedTip === null) {
      setCustomTipValue(null);
    }
  }, [selectedTip]);

  return (
    <Stack gap={5} height="100%" pb={3}>
      <Input
        value={bill === 0 ? "" : bill}
        onChange={(e) => setBill(Number(e.target.value))}
        icon={DollarIcon}
        label="Bill"
        name="bill"
        type="number"
        id="bill"
        placeholder="0"
        min={0}
      />
      <Box>
        <Typography fontWeight={700} color={"primary.800"} fontSize={20} mb={2}>
          Select Tip %
        </Typography>
        <Grid columns={3} spacing={2} container>
          {TIPS.map((tip) => (
            <Grid item xs={1} key={tip}>
              <TipButton
                onSelectTip={handleSelectTip}
                tip={tip}
                isActive={!customTipValue && tip === selectedTip}
              />
            </Grid>
          ))}
          <Grid item xs={1}>
            <CustomTip
              value={customTipValue?.toString() ?? ""}
              onChangeTip={handleCustomTipChange}
              placeholder="Custom"
            />
          </Grid>
        </Grid>
      </Box>
      <Input
        value={numberOfPeople}
        onChange={(e) => setNumberOfPeople(Number(e.target.value))}
        icon={PersonIcon}
        type="number"
        id="people"
        name="people"
        label="Number of People"
        placeholder="0"
        min={1}
      />
    </Stack>
  );
};

export default TipForm;